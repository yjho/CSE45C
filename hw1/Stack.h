#include<stdlib.h>
#include<iostream>
#define STACK_CAPACITY 1000

class Stack {
    private:
        char data[STACK_CAPACITY];
        char *loc;

        void error(const char *msg){
            std::cerr << std::endl <<"ERROR : " << msg << std::endl;
            exit(-1);
        }
    public:

        Stack()
            : loc(data)
        { }

        void push(char c){
            if (isFull()){
                error("Stack is full");
            }
            *(loc++) = c;
        }

        char pop(){
            if (isEmpty()){
                error("Stack is empty");
            }
            char temp = *(--loc);
            return temp;
        }

        char top(){
            return *loc;
        }

        bool isEmpty(){
            return loc == &data[0];
        }

        bool isFull(){
            return loc == &data[STACK_CAPACITY -1];
        }

        ~Stack(){
            loc = 0;
        };
};
