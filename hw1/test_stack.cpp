#include"Stack.h"
#include<iostream>
using namespace std;

int main(int argc,char *argv[]){
    string line;
    Stack rev = Stack();
    while(getline(cin,line)){
        for(int i = 0;i < line.length();i++){
            rev.push(line[i]);
        }
        while(!rev.isEmpty()){
            cout << rev.pop();
        }
        cout << endl;
    }
}
