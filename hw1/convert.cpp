#include<iostream>
using namespace std;

double convert(int knot){
    double mph = (knot * 6076.0) / 5280.0;
    double res = mph / 60;
    return res;
}

int main(int argc, char *argv[]){
    int speed = 0;
    cout << "Enter speed in knots: " ;
    cin >> speed;
    cout << speed << " knots is " <<convert(speed) << " miles per minute " << endl;
}
