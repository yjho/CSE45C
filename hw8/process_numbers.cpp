#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include <algorithm>
using namespace std;

int main(){
    ifstream nums("rand_numbers.txt");
    ofstream odd("odd.txt");
    ofstream even("even.txt");
    vector<int> values;

    copy(istream_iterator<int>(nums),istream_iterator<int>(),back_inserter(values));
    sort(begin(values),end(values),less<int>());

    for_each(begin(values),end(values),[&](int i){
            if(abs(i) % 2 == 1){
                odd << i << ' ';
            } else {
                even << i << endl;
            }
            });
}
