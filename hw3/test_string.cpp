#include <iostream>
#include "String.h"
using namespace std;

void test_assignment(const char *t1){
    String m = t1;
    String s;
    cout << "Assignment: " ;
    m = s;
    cout << m << " and " << s << endl;
}

void test_index(const char *t1, int index){
    String m = t1;
    cout << "Indexing: " ;
    cout << index << " " << m[index] << endl;
}

void test_size(const char *t1){
    String m = t1;
    cout << "Size: " ;
    cout << m.size() << endl;
}

void test_reverse(const char *t1){
    String m = t1;
    cout << "Reverse: " ;
    cout << m.reverse() << endl;
}

void test_indexof_char(const char *t1,int index){
    String m = t1;
    cout << "IndexOf char: " ;
    cout << m[index] << " " << m.indexOf(m[index]) << endl;
}

void test_indexof_str(const char *t1,const char *t2){
    String m = t1;
    cout << "IndexOf str: " ;
    cout << t2 << " " <<   m.indexOf(t2) << endl;
}

void test_equality(const char *t1, const char *t2){
    String m = t1;
    String s = t2;
    cout << "Equality: ";
    cout << t1 << " " << t2 << " " << (m==s) << endl;
}

void test_inequality(const char *t1, const char *t2){
    String m = t1;
    String s = t2;
    cout << "InEquality: ";
    cout << t1 << " " << t2 << " " << (m!=s) << endl;
}

void test_gt(const char *t1, const char *t2){
    String m = t1;
    String s = t2;
    cout << "Greater: ";
    cout << t1 << " " << t2 << " " << (m>s) << endl;
}

void test_lt(const char *t1, const char *t2){
    String m = t1;
    String s = t2;
    cout << "Less: ";
    cout << t1 << " " << t2 << " " << (m<s) << endl;
}

void test_gte(const char *t1, const char *t2){
    String m = t1;
    String s = t2;
    cout << "Greater Equal: ";
    cout << t1 << " " << t2 << " " << (m>=s) << endl;
}

void test_lte(const char *t1, const char *t2){
    String m = t1;
    String s = t2;
    cout << "Less Equal: ";
    cout << t1 << " " << t2 << " " << (m<=s) << endl;
}

void test_addition(const char *t1,const char *t2){
    String m = t1;
    String s = t2;
    cout << "Addition: ";
    cout << m+s << endl;
}

void test_addition_assign(const char *t1,const char *t2){
    String m = t1;
    String s = t2;
    cout << "Addition Assign: ";
    m += s;
    cout << m << endl; 
}

void test_read(){
    String m;
    cout << "Input: ";
    m.read(cin);
    cout << "Readback: " << m << endl;
}

int main(){
    const int num_tests = 3;
    char negative[] = "Foobar";
    char tests[num_tests][128] = {"Watashi","Who are you?","THis string sho"};
    for(int i = 0; i < num_tests;i++){
        cout << "Testing: " << tests[i] << endl;
        test_assignment(tests[i]);
        test_index(tests[i],3);
        test_size(tests[i]);
        test_reverse(tests[i]);
        test_indexof_char(tests[i],3);
        test_indexof_str(tests[i],"are");
        test_equality(tests[i],tests[i]);
        test_inequality(tests[i],negative);
        test_gt(tests[i],negative);
        test_lt(tests[i],negative);
        test_gte(tests[i],tests[i]);
        test_gte(tests[i],negative);
        test_lte(tests[i],tests[i]);
        test_lte(tests[i],negative);
        test_addition(tests[i],negative);
        test_addition_assign(tests[i],negative);
        cout << endl;
    }

    test_read();
}
