#include <iostream>
#include <fstream>
#include <iterator>
#include <string>
#include <functional>
#include <algorithm>
#include "SetList.h"
using namespace std;

int main(){
    SetList<string> exclusionList;
    
    ifstream ex("stopwords.txt");
    copy(istream_iterator<string>(ex),istream_iterator<string>(),front_inserter(exclusionList));

    for(auto i:exclusionList){
        cout << i << endl;
    }
    return 0;
}
