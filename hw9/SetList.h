#ifndef SETLIST_H
#define SETLIST_H

#include<iterator>
using namespace std;

template <typename T> class SetList;

template <typename T>
struct ListNode{
    friend class SetList<T>;

    private:
        T info;
        ListNode<T> *next;

        ListNode(){
            next = nullptr;
        }
        ListNode(const T& info,ListNode *next):info(info),next(next){}

        static ListNode<T> * end(ListNode<T> *L){
            if(L){
                return end(L->next);
            } else {
                return L;
            }
        }

        static int length(ListNode<T> *L){
            if(L){
                return 1+length(L->next);
            } else {
                return 0;
            }
        }

        ~ListNode(){
            delete next;
        }
};


template <typename T>
class SetList{
    private:
        ListNode<T> *head;
    public:
        typedef T value_type;

        struct iterator {
            public:
                typedef forward_iterator_tag iterator_category;
                typedef T value_type;
                typedef iterator self_type;
                typedef T& reference;
                typedef T* pointer;
                typedef ptrdiff_t difference_type;
            private:
                ListNode<T>* head;

            public:
                iterator(ListNode<T>* ptr):head(ptr){}
                
                self_type operator++(){
                    if(head){
                        head=head->next;
                    }
                    return *this;
                }

                self_type operator++(int postfix){
                    self_type cpy = *this;
                    if(head){
                        head = head->next;
                    }
                    return cpy;
                }

                reference operator*(){
                    return head->info;
                }

                pointer operator->(){
                    return *head->info;
                }

                bool operator==(const self_type& rhs) const {
                    return head == rhs.head;
                }

                bool operator!=(const self_type& rhs) const {
                    return head != rhs.head;
                }
        };

        SetList():head(nullptr){}
        SetList(T item):head(new ListNode<T>(item,nullptr)){}

        SetList(initializer_list<T> s):head(nullptr){
            for (auto M : s){
                if(find(M) == end()){
                    ListNode<T> *p = new ListNode<T>(M,head);
                    head = p;
                }
            }
        }

        iterator find(const T& t){
            iterator i = begin();
            for(;i != end();++i){
                if (*i == t){
                    break;
                }
            }
            return i;
        }

        void push_front(const T& it){
            if(find(it) == end()){
                ListNode<T> *newNode = new ListNode<T>(it,head);
                head = newNode;
            }
        }

        int size() const {
            return ListNode<T>::length(head);
        }

        iterator begin() {
            return iterator(head);
        }

        iterator end(){
            return iterator(nullptr);
        }

//        const_iterator begin() const{
//            return const_iterator(head);
//        }
//
//        const_iterator end() const{
//            return const_iterator(tail());
//        }

        ~SetList(){
            delete head;
        }


};

#endif
