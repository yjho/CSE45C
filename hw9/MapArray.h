#ifndef MAPARRAY_H
#define MAPARRAY_H
#define START_SIZE 128

#include <iterator>
#include <iostream>
using namespace std;

template <typename K,typename V>
class MapArray{
    private:
        int len;
        pair<K,V> *map;

    public:
        struct iterator {
            public:
                typedef random_access_iterator_tag iterator_category;
                typedef pair<K,V> value_type;
                typedef iterator self_type;
                typedef pair<K,V>& reference;
                typedef pair<K,V>* pointer;
                typedef ptrdiff_t difference_type;

            private:
                pair<K,V> *head;

            public:
                iterator(pair<K,V> *h):head(h){
                }

                self_type operator++(){
                    ++head;
                    return *this;
                }

                self_type operator++(int postfix){
                    self_type copy = *this;
                    ++head;
                    return copy;
                }

                self_type operator--(){
                    --head;
                    return *this;
                }

                self_type operator--(int postfix){
                    self_type copy = *this;
                    --head;
                    return copy;
                }

                reference operator[] (const difference_type& n ) const {
                    return head[n];
                }

                reference operator*(){
                    return *head;
                }

                pointer operator->(){
                    return head;
                }

                bool operator==(const self_type& rhs) const {
                    return head == rhs.head;
                }

                bool operator!=(const self_type& rhs) const {
                    return head != rhs.head;
                }
        };

        MapArray():len(0),map(new pair<K,V>[START_SIZE]){
        }


        // Returns an iterator of the location that was inserted
        void insert(const pair<K,V>& ins){
            // If len is approaching our start size we double the size of our array
            pair<K,V> *m = new pair<K,V>[2+len+START_SIZE];
            pair<K,V> *p = m;
            bool inserted = false;
            iterator i = begin();
            while(i != end()){
                if((i->first <= ins.first && i->first != "") || inserted){
                    *p = *i;
                    ++p;
                    ++i;
                }else{
                    *p = ins;
                    inserted = true;
                    ++p;
                }            
            }
            delete[] map;
            map = m;
        }

        V& operator[](K key){
            iterator m = find(key);
            if(m == end()){
                ++len;
                pair<K,V> ins;
                ins.first = key;
                insert(ins);
                m = find(key);
            }
            return m->second;
        }

        iterator find(K key){
            iterator i = begin();
            iterator e = end();
            while(i != e){
                if(i->first == key){
                    return i;
                }
                ++i;
            }
            return i;
        }

        iterator begin(){
            return iterator(map);
        }

        iterator end(){
            return iterator(map+len);
        }

        ~MapArray(){
            delete []map;
        }
};

#endif
