#ifndef MATRIX_H
#define MATRIX_H
#include<iostream>
#include"Array.h"
using namespace std;

template<typename T>
class Matrix {
    private:
        int rows,cols;
        Array<Array<T>*> m;

        virtual bool inBounds(int i){
            return (i >= 0 && i < rows);
        }
    public:
        class IndexOutOfBoundsException{};

        Matrix(int newRows,int newCols)
            :rows(newRows),cols(newCols),m(rows)
        {
            for(int i=0;i <rows;i++){
                m[i] = new Array<T>(cols);
            }
        }

        int numRows(){
            return rows;
        }

        int numCols(){
            return cols;
        }

        Array<T> &operator [] (int row){
            if(!inBounds(row))
                throw IndexOutOfBoundsException();
            return *m[row];
        }

        void print(ostream &out){
            for(int i=0; i<rows; ++i){
                out << m[i] << endl;
            }
        }

        friend ostream &operator << (ostream &out, Matrix &m){
            m.print(out);
            return out;
        }

};

#endif
