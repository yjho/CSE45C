#include"Matrix.h"

template<typename T>
void fillMatrix(Matrix<T> &m){
    int i,j;
    for(i=0;i <m.numRows();++i)
        m[i][0] = T();
    for(j=0;j<m.numCols();++j)
        m[0][j] = T();
    for(i=1; i<m.numRows();++i){
        for(j=1;j<m.numCols();++j){
            m[i][j] = T(i*j);
        }
    }
}

void test_int_matrix(){
    Matrix<int> m(10,5);
    fillMatrix(m);
    cout << m;

    try{
        //m[-1];
        m[100];
    } catch(Matrix<int>::IndexOutOfBoundsException &e){
        cout << "Caught an int OOB exception" << endl;
    }
}

void test_double_matrix(){
    Matrix<double> M(8,10);
    fillMatrix(M);
    cout << M;
    try{
        //M[-1];
        M[100];
    } catch(Matrix<double>::IndexOutOfBoundsException &e){
        cout << "Caught a double OOB exception" << endl;
    }
}

int main(){
    test_int_matrix();
    test_double_matrix();
    return 0;
}
