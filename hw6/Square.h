#ifndef SQUARE_H
#define SQUARE_H
#include "Rectangle.h"
#include <iostream>
using namespace std;

class Square : public Rectangle {
    public:
        Square(int x,int y,int h)
            :Rectangle(x,y,h,h){}

        void draw(){
            cout <<"*******\n*     *\n*     *\n*     *\n*     *\n*******\n" << endl;
        }

    protected:
        const string name = "Square";
        int height;
};
#endif
