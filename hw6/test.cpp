#include "Shape.h"
#include "Triangle.h"
#include "Square.h"
#include "Circle.h"
#include <iostream>
using namespace std;

void test_triangle(){
    Triangle t(1,1,9,3);
    t.draw();
    cout <<t.area() << endl;
}

void test_square(){
    Square s(1,1,20);
    s.draw();
    cout << s.area() << endl;
}

void test_circle(){
    Circle c(1,1,10);
    c.draw();
    cout << c.area() << endl;
}

int main(){
    test_triangle();
    test_square();
    test_circle();
    return 0;
}

