#include "Shape.h"
#include "Circle.h"
#include "Picture.h"
#include "Square.h"
#include "Rectangle.h"
#include "Triangle.h"
#include <iostream>
using namespace std;

int main(){
    Triangle *t = new Triangle(1,1,5,10);
    Circle *c = new Circle(1,1,10);
    Square *s = new Square(1,1,4);
    Rectangle *r = new Rectangle(1,1,4,5);

    Picture *pic = new Picture();
    pic->add(t);
    pic->add(c);
    pic->add(s);
    pic->add(r);
    pic->drawAll();
    cout << pic->totalArea();
    delete pic;
    delete t;
    delete c;
    delete s;
    delete r;
    return 0;
}
