#ifndef CIRCLE_H
#define CIRCLE_H
#include "Shape.h"
#include <string>
#include <iostream>
using namespace std;

#define PI 3.14159265359

class Circle : public Shape {
    public: 
        Circle(int x,int y,int r)
        :Shape("Circle",x,y)
        ,radius(r){}

        double area(){
            return PI * radius * radius;
        }

        void draw(){
            cout << "  ***  \n *   * \n*     *\n*     *\n*     *\n *   * \n  ***  \n" << endl;
        }

    private:
        int radius;
};

#endif
