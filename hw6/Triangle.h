#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Shape.h"
#include <string>
#include <iostream>
using namespace std;

class Triangle : public Shape {
    public:
        Triangle(int x, int y,int b,int h)
            :Shape("Triangle",x,y)
            ,base(b)
            ,height(h){
        }

        double area(){
            return 0.5 * base * height;
        }

        void draw(){
            cout << "      *\n     **\n    * *\n   *  *\n  *   *\n *    *\n*******\n" << endl;
        }
    private:
        int base;
        int height;

};

#endif
