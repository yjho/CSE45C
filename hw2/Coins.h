#include <iostream>
using namespace std;

const int QUARTER = 25;
const int DIME = 10;
const int NICKEL = 5;
const int PENNY = 1;

class Coins {
    public:
        Coins(int q, int d, int n, int p)
        :quarters(q),dimes(d),nickels(n),pennies(p){
        }

        void depositChange(Coins &c){
            quarters += c.quarters;
            dimes += c.dimes;
            nickels += c.nickels;
            pennies += c.pennies;
        }

        bool hasSufficientAmount(int amount){
            return value() >= amount;
        }

        Coins extractChange(int amount){
            int coins[4] = {0,0,0,0};
            int values[4] = {QUARTER,DIME,NICKEL,PENNY};
            int it[4] = {quarters,dimes,nickels,pennies};

            for(int i = 0; i<=3; i++){
                while(it[i] > 0 && amount >= values[i]){
                    amount -= values[i];
                    coins[i]++;
                }
            }

            quarters -= coins[0];
            dimes -= coins[1];
            nickels -= coins[2];
            pennies -= coins[3];
            cout << quarters;
            return Coins(coins[0],coins[1],coins[2],coins[3]);
        }

        void print(ostream &out){
            out << value() <<" cents " << " Q:"<<quarters << " D:"<< dimes 
                << " N:"<<nickels << " P:" << pennies << endl;
        }

            int value(){ 
                return quarters*QUARTER + dimes*DIME + nickels*NICKEL + pennies;
            }
    private:
        int quarters, dimes, nickels, pennies;
};

ostream& operator << (ostream& out, Coins& c){
    //out << "Q: " << c.quarters;
    c.print(out);
    return out;
}
