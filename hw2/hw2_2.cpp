#include "Coins.h"

const int CHIPS = 68;

int main() {
    Coins pocket(5,3,6,8);
    Coins piggyBank(50,50,50,50);

    cout << "Buying a bag of chips: " << CHIPS << endl;
    pocket.extractChange(68);
    cout << "Pocket: " << pocket << endl << endl;

    cout << "Getting 2 dollars from piggy bank " << endl;
    Coins temp = piggyBank.extractChange(200);
    pocket.depositChange(temp);
    cout << "Pocket: " << pocket << endl;
    cout << "Piggy Bank: "<< piggyBank << endl;

    Coins spareChange(1,1,2,3);
    cout << "Found " << spareChange << " behind the couch!" << endl;
    piggyBank.depositChange(spareChange);
    cout << "Piggy Bank: "<< piggyBank << endl;

}
