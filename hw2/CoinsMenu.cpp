#include "Coins.h"
#include<limits>

int getValidInput(istream &in);

int main(){
    Coins myCoins(0,0,0,0);
    int input = 0;
    int coinInp = 0;
    int run = 1;

    while(run > 0){
        cout << "----- Menu -----"<< endl;
        cout << "1) Withdraw" << endl;
        cout << "2) Deposit " << endl;
        cout << "3) Balance " << endl;
        cout << "4) Exit    " << endl;
        cout << "-----------------" << endl << endl;

        input =getValidInput(cin);
        switch(input){
            case 1: // Extract change
                cout << "Withdraw coins " << endl;
                coinInp = getValidInput(cin);
                if (myCoins.hasSufficientAmount(coinInp)){
                    myCoins.extractChange(coinInp);
                    cout << "Pocket:    " << myCoins;
                } else {
                    cout << "Insufficient funds" << endl;
                }
                break;
            case 2: { // Deposit change
                int q,d,n,p;
                cout << "Deposit coins " << endl;
                cout << "Q: ";
                q = getValidInput(cin);
                cout << "D: ";
                d = getValidInput(cin);
                cout << "N: ";
                n = getValidInput(cin);
                cout << "P: ";
                p = getValidInput(cin);
                cout << endl;
                Coins *temp = new Coins(q,d,n,p);
                myCoins.depositChange(*temp);
                cout << "Pocket:   " << myCoins;
                delete temp;
                break;
                }
            case 3: // Balance
                cout << "Balance " << endl;
                myCoins.print(cout);
                break;
            case 4:
                run = -1;
                cout << "Bye" << endl;
                break;
            default:
                cout << "Please enter valid input" << endl;
        }
        cout << endl;
    }
    return 0;
}

int getValidInput(istream &in){
    int inp = 0;
    cout << "-> ";
    in >> inp;
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    if(inp >= 0){
        return inp;
    }else {
        return 0;
    }

}
